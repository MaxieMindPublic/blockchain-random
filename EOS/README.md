<!-- TITLE: RNG on EOS (ETH) -->
<!-- SUBTITLE: The main idea of EOS (ETH) Random Number Generator -->

# Idea

1. The **server** generates the **seed** for each game and gives **hash** of this seed to the user.
2. The **user** generates his own **seed**, selects the size of the bet and sends this data to the server.
3. The server receives the **winning combination** with help of an open mathematical algorithm using own **server’s seed** and the **user’s seed**.
4. The user gets a **winning combination** at the end of the game .
5. The user also receives the **server's seed** and can verify its authenticity using the previously obtained **hash** of this **seed**.
6. The user can get lost in the correctness of the **winning combination** trying to remember **user’s seed** and the **server’s seed**.

# Global variables

Contract global variables

```
const uint64_t CASINO_COMMISSION = 240;
const uint64_t CASINO_REF_COMMISSION = 140;
const uint64_t REF_REWARD = 100;
```

If there is no referral, the casino commission is 2.40%. Otherwise, the referral receives 1.00% and the casino receives - 1.40%.

```
const uint64_t MIN_AMOUNT = 1000;
```

Minimum bet.

# Public functions

## initcontract

```
public void initcontract(public_key randomness_key)
```

Initialization.
Only **wincasinoeos**.

Adds **Game id**, **Total amount**, **Total won amount**, **Liabilities** to *globalvars*.
Add **randomness_key** to *randkeys*.

## updaterandkey

```
void updaterandkey(public_key randomness_key)
```

Updates *randkeys*.
Available only for **wincasinoeos**.

## bet

```
void bet()
```

Creates bet.
Available only for **wincasinoeos**.

Bet must be bigger than *MIN_AMOUNT*.

*memo: "{roll_str}-{ref_str}-{seed_str}"*

The casino and referral commission is calculated using global variables.

**roll_under** - user's rate (After falling dice can make a combination from 0 to 100. If the combination is less than rate, the user wins, if it is bigger he loses).
*2 <= roll_under <= 96*

## refundbet

```
void refundbet(const uint64_t bet_id)
```

Checks the elapsed time from the moment when the bet was created.
Available only for **wincasinoeos** with **random** permission.

## checkbet

```
void checkbet(const uint64_t bet_id, signature sig)
```

Calculates and sends winnings.
Available only for **wincasinoeos** with **random** permission.

**seed** - seed of this bet from *activebets*
**sig** - server's seed
**pub** - public key from *randkeys*

Tests a given **pub** with the generated key from **seed** and the **sig**.

**random_roll** - RNG result.

If *random_roll <= roll_under* the user wins.
After that, the winnings are sent to the user and referral. If the user loses, the bet is sent to the casino wallet.

## removebet

```
void removebet(const uint64_t bet_id)
```

Deletes bet by *bet_id*.
Available only for **wincasinoeos**.

## betsender

```
void betsender(uint64_t bet_id, account_name bettor, account_name amt_contract, asset bet_amt, asset payout, checksum256 seed, signature signature, uint64_t roll_under, uint64_t random_roll)
```

Checks bet sender.
Available only for **wincasinoeos**.

# Private functions
## name_to_string

```
string name_to_string(uint64_t acct_int) const
```

Transforms user's name into string.

## increment_liabilities

```
void increment_liabilities(const uint64_t bet_amt)
```

Increments global variable *liabilities*.

## decrement_liabilities

```
void decrement_liabilities(const uint64_t bet_amt)
```

Decrements global variable *liabilities*.

## increment_game_stats

```
void increment_game_stats(const uint64_t bet_amt, const uint64_t won_amt)
```

Decrements global variables *Total amount* and *Total won amount*.

## airdrop

```
void airdrop(const uint64_t bet_id, const uint64_t bet_amt, const account_name bettor)
```

Airdrops tokens.

## get_balance

```
uint64_t get_balance(const account_name token_contract, const symbol_type &token_type) const
```

Returns token balance.

## get_payout

```
get_payout(const uint64_t roll_under, const uint64_t commission) const
```

## get_max_win

```
uint64_t get_max_win() const
```

User can't win more than *(eos_balance - liabilities) / 25*.

# Structs
## bet

```
struct bet {
	uint64_t id;
	account_name bettor;
	account_name referral;
	uint64_t bet_amt;
	uint64_t roll_under;
	checksum256 seed;
	time_point_sec bet_time;

	uint64_t primary_key() const { return id; }

	EOSLIB_SERIALIZE(bet, (id)(bettor)(referral)(bet_amt)(roll_under)(seed)(bet_time))
};
```

Struct of bet object.

## globalvar

```
struct globalvar {
	uint64_t id;
	uint64_t val;

	uint64_t primary_key() const { return id; }

	EOSLIB_SERIALIZE(globalvar, (id)(val));
};
```

Struct of globalvar object.

## randkey

```
struct randkey {
	uint64_t id;
	public_key key;

	uint64_t primary_key() const { return id; }
};
```

Struct of randkey object.
