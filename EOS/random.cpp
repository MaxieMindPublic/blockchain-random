#include <boost/algorithm/string.hpp>
#include <eosiolib/eosio.hpp>
#include <eosiolib/time.hpp>
#include <eosiolib/asset.hpp>
#include <eosiolib/contract.hpp>
#include <eosiolib/types.hpp>
#include <eosiolib/transaction.hpp>
#include <eosiolib/crypto.h>
#include <string>
#include <utility>
#include <vector>

using eosio::action;
using eosio::asset;
using eosio::name;
using eosio::permission_level;
using eosio::print;
using eosio::symbol_type;
using eosio::time_point_sec;
using eosio::transaction;
using eosio::unpack_action_data;

using namespace std;

class WinCasinoEOS : public eosio::contract {
public:
	const uint64_t CASINO_COMMISSION 			= 240; // 2.4% (1.4% + 1.0%)
	const uint64_t CASINO_REF_COMMISSION 	= 140; // 1.4%
	const uint64_t REF_REWARD 						= 100; // 1.0%

	const uint64_t MIN_AMOUNT = 1000;

	WinCasinoEOS(account_name self) : eosio::contract(self),
																	activebets(_self, _self),
																	globalvars(_self, _self),
																	randkeys(_self, _self) {}

	// Initialization
	// @abi action
	void initcontract(public_key randomness_key) {
		require_auth(N(wincasinoeos));

		auto globalvars_itr = globalvars.begin();
		eosio_assert(globalvars_itr == globalvars.end(), "Contract is init successfully");

		globalvars.emplace(_self, [&](auto &g) { g.id = 1; g.val = 0; }); // Game id
		globalvars.emplace(_self, [&](auto &g) { g.id = 2; g.val = 0; }); // Total amount
		globalvars.emplace(_self, [&](auto &g) { g.id = 3; g.val = 0; }); // Total won amount
		globalvars.emplace(_self, [&](auto &g) { g.id = 4; g.val = 0; }); // Liabilities

		randkeys.emplace(_self, [&](auto &k) { k.id = 1; k.key = randomness_key; });
	}

	// Update randkeys
	// @abi action
	void updaterandkey(public_key randomness_key) {
		require_auth(N(wincasinoeos));
		auto rand_itr = randkeys.begin();
		randkeys.modify(rand_itr, _self, [&](auto &k) { k.key = randomness_key; });
	}

  // Create bet
	// @abi action
	void bet() {
		auto bet_data = unpack_action_data<st_bet>();
		if (bet_data.from == _self || bet_data.from == N(wincasinoeos))
			return;

		eosio_assert(bet_data.quantity.is_valid(), "Invalid asset");
		const uint64_t bet_amount = (uint64_t)bet_data.quantity.amount;
		eosio_assert(MIN_AMOUNT <= bet_amount, "Too low amount");
		increment_liabilities(bet_amount);

		string bet_str;
		string ref_str;
		string seed_str;

		const size_t first_break = bet_data.memo.find("|");
		bet_str = bet_data.memo.substr(0, first_break);

		if (first_break != string::npos) {
			const string after_first_break = bet_data.memo.substr(first_break + 1);
			const size_t second_break = after_first_break.find("|");

			if (second_break != string::npos) {
				ref_str = after_first_break.substr(0, second_break);
				seed_str = after_first_break.substr(second_break + 1);
			} else {
				ref_str = after_first_break;
				seed_str = string("");
			}
		}
		else {
			ref_str = string("");
			seed_str = string("");
		}

		account_name referral = N(wincasinoeos);

		const account_name possible_ref = eosio::string_to_name(ref_str.c_str());
		uint64_t commission = CASINO_COMMISSION;

		if (possible_ref != _self &&
				possible_ref != bet_data.from &&
				is_account(possible_ref)) {
			referral = possible_ref;
			commission = CASINO_REF_COMMISSION;
		}

		// roll_under - user's rate
		// After falling dice can make a combination from 0 to 100.
		// If the combination is less than rate, the user wins, if it is bigger he loses.
		const uint64_t roll_under = stoull(bet_str, 0, 10); 
		eosio_assert(roll_under >= 5 && roll_under <= 94, " 5 <= Roll <= 94");

		const uint64_t win_amount = (bet_amount * get_payout(roll_under, commission) / 10000) - bet_amount;
		eosio_assert(win_amount <= get_max_win(), "less than max win");

		checksum256 user_seed_hash;
		sha256((char *)&seed_str, seed_str.length(), &user_seed_hash);

		auto s = read_transaction(nullptr, 0);
		char *tx = (char *)malloc(s);
		read_transaction(tx, s);
		checksum256 tx_hash;
		sha256(tx, s, &tx_hash);

		st_seeds seeds;
		seeds.firstseed = user_seed_hash;
		seeds.secondseed = tx_hash;

		checksum256 seed_hash;
		sha256((char *)&seeds.firstseed, sizeof(seeds.firstseed) * 2, &seed_hash);

		uint64_t bet_id = (uint64_t)tx_hash.hash[7];
		for (int i = 0; i < 7; i++) {
			bet_id += ((uint64_t)tx_hash.hash[i] << 8 * (7 - i));
		}

		activebets.emplace(_self, [&](auto &bet) {
			bet.id = bet_id;
			bet.bettor = bet_data.from;
			bet.referral = referral;
			bet.bet_amt = bet_amount;
			bet.roll_under = roll_under;
			bet.seed = seed_hash;
			bet.bet_time = time_point_sec(now());
		});
	}

	// Checks the elapsed time from the moment when the bet was created.
	// @abi action
	void refundbet(const uint64_t bet_id) {
		require_auth2(N(wincasinoeos), N(random));

		auto activebets_itr = activebets.find(bet_id);
		eosio_assert(activebets_itr != activebets.end(), "Game doesn't exist");

		const time_point_sec bet_time = activebets_itr->bet_time;
		eosio_assert(time_point_sec(now() - (2 * 60)) > bet_time, "Wait 2 minutes");
		decrement_liabilities(activebets_itr->bet_amt);

		action(
			permission_level{_self, N(active)},
			N(eosio.token),
			N(bet),
			make_tuple(
					_self,
					N(safetransfer),
					asset(activebets_itr->bet_amt, symbol_type(S(4, EOS))),
					name_to_string(activebets_itr->bettor) + string(" Bet id: ") + to_string(bet_id) + string(" - nope - ")))
			.send();
		activebets.erase(activebets_itr);
	}

	// Calculates and sends winnings.
	// @abi action
	void checkbet(const uint64_t bet_id, signature sig) {
		require_auth2(N(wincasinoeos), N(random));

		auto activebets_itr = activebets.find(bet_id);
		eosio_assert(activebets_itr != activebets.end(), "Wrong bet_id");

		auto key_entry = randkeys.get(1);
		public_key rand_signing_key = key_entry.key;

		// seed - seed of this bet from activebets
		// sig - server's seed
		// pub - public key from randkeys
		// Tests a given pub with the generated key from seed and the sig.
		assert_recover_key(&activebets_itr->seed, (const char *)&sig, sizeof(sig), (const char *)&rand_signing_key, sizeof(rand_signing_key)); // Idea

		checksum256 random_num_hash;
		sha256((char *)&sig, sizeof(sig), &random_num_hash);

		// RNG result.
		uint64_t random_roll = random_num_hash.hash[7];
		for (int i = 0; i < 7; i++) {
			random_roll += random_num_hash.hash[i];
		}
		random_roll = (random_roll % 100) + 1;

		uint64_t edge = CASINO_COMMISSION;
		uint64_t ref_reward = 0;
		uint64_t payout = 0;
		if (activebets_itr->referral != N(wincasinoeos)) {
			edge = CASINO_REF_COMMISSION;
			ref_reward = activebets_itr->bet_amt * REF_REWARD / 10000;
		}

		if (random_roll < activebets_itr->roll_under) {
			payout = (activebets_itr->bet_amt * get_payout(activebets_itr->roll_under, edge)) / 10000;
		}

		increment_game_stats(activebets_itr->bet_amt, payout);
		decrement_liabilities(activebets_itr->bet_amt);

		if (payout > 0) {
			action(
				permission_level{_self, N(active)},
				N(eosio.token),
				N(bet),
				make_tuple(
						_self,
						activebets_itr->bettor,
						asset(payout, symbol_type(S(4, EOS))),
						string("Bet id: ") + to_string(bet_id) + string(" - winner -")))
				.send();
		}

		transaction ref_tx{};

		ref_tx.actions.emplace_back(
			permission_level{_self, N(active)},
			_self,
			N(betsender),
			make_tuple(
					bet_id,
					activebets_itr->bettor,
					N(eosio.token),
					asset(activebets_itr->bet_amt, symbol_type(S(4, EOS))),
					asset(payout, symbol_type(S(4, EOS))),
					activebets_itr->seed,
					sig,
					activebets_itr->roll_under,
					random_roll));

		if (ref_reward > 0) {
			ref_tx.actions.emplace_back(
				permission_level{_self, N(active)},
				N(eosio.token),
				N(bet),
				make_tuple(
						_self,
						N(safetransfer),
						asset(ref_reward, symbol_type(S(4, EOS))),
						name_to_string(activebets_itr->referral) + string(" Bet id: ") + to_string(bet_id) + string(" - referral - ")));
		}

		ref_tx.delay_sec = 5;
		ref_tx.send(bet_id, _self);
		airdrop(bet_id, activebets_itr->bet_amt, activebets_itr->bettor);
		activebets.erase(activebets_itr);
	}

	// Deletes bet by bet_id.
	// @abi action
	void removebet(const uint64_t bet_id) {
		require_auth(N(wincasinoeos));

		auto activebets_itr = activebets.find(bet_id);
		eosio_assert(activebets_itr != activebets.end(), "Wrond bet_id");

		string bettor_str = name_to_string(activebets_itr->bettor);
		decrement_liabilities(activebets_itr->bet_amt);

		action(
			permission_level{_self, N(active)},
			N(eosio.token),
			N(bet),
			make_tuple(
					_self,
					N(eosbettransf),
					asset(activebets_itr->bet_amt, symbol_type(S(4, EOS))),
					bettor_str))
			.send();

		activebets.erase(activebets_itr);
	}

	// Checks bet sender.
	// @abi action
	void betsender(
		uint64_t bet_id,
		account_name bettor,
		account_name amt_contract,
		asset bet_amt,
		asset payout,
		checksum256 seed,
		signature signature,
		uint64_t roll_under,
		uint64_t random_roll) {

		require_auth(N(wincasinoeos));
		require_recipient(bettor);
	}

private:
	// Struct of bet object.
	// @abi table activebets i64
	struct bet {
		uint64_t id;
		account_name bettor;
		account_name referral;
		uint64_t bet_amt;
		uint64_t roll_under;
		checksum256 seed;
		time_point_sec bet_time;

		uint64_t primary_key() const { return id; }

		EOSLIB_SERIALIZE(bet, (id)(bettor)(referral)(bet_amt)(roll_under)(seed)(bet_time))
	};

	typedef eosio::multi_index<N(activebets), bet> bets_index;

	// Struct of globalvar object.
	// @abi table globalvars i64
	struct globalvar {
		uint64_t id;
		uint64_t val;

		uint64_t primary_key() const { return id; }

		EOSLIB_SERIALIZE(globalvar, (id)(val));
	};

	typedef eosio::multi_index<N(globalvars), globalvar> globalvars_index;

	// Struct of randkey object.
	// @abi table randkeys i64
	struct randkey {
		uint64_t id;
		public_key key;

		uint64_t primary_key() const { return id; }
	};

	typedef eosio::multi_index<N(randkeys), randkey> randkeys_index;

	// taken from eosio.token.hpp
	struct account {
		asset balance;

		uint64_t primary_key() const { return balance.symbol.name(); }
	};

	typedef eosio::multi_index<N(accounts), account> accounts;

	// taken from eosio.token.hpp
	struct st_bet {
		account_name from;
		account_name to;
		asset quantity;
		string memo;
	};
	struct st_seeds {
		checksum256 firstseed;
		checksum256 secondseed;
	};

	bets_index activebets;
	globalvars_index globalvars;
	randkeys_index randkeys;

	// Transforms user's name into string.
	string name_to_string(uint64_t acct_int) const {
		static const char *charmap = ".12345abcdefghijklmnopqrstuvwxyz";

		string str(13, '.');
		uint64_t tmp = acct_int;

		for (uint32_t i = 0; i <= 12; ++i) {
			char c = charmap[tmp & (i == 0 ? 0x0f : 0x1f)];
			str[12 - i] = c;
			tmp >>= (i == 0 ? 4 : 5);
		}

		boost::algorithm::trim_right_if(str, [](char c) { return c == '.'; });
		return str;
	}

	// Increments global variable liabilities.
	void increment_liabilities(const uint64_t bet_amt) {
		auto globalvars_itr = globalvars.find(1); // 1 - Bet index
		globalvars.modify(globalvars_itr, _self, [&](auto &g) { g.val++; });

		globalvars_itr = globalvars.find(4); // Liabilities amount
		globalvars.modify(globalvars_itr, _self, [&](auto &g) { g.val += bet_amt; });
	}
	// Decrements global variable liabilities.
	void decrement_liabilities(const uint64_t bet_amt) {
		auto globalvars_itr = globalvars.find(4); // Liabilities amount
		globalvars.modify(globalvars_itr, _self, [&](auto &g) { g.val -= bet_amt; });
	}

	// Decrements global variables 'Total amount' and Total won amount'.
	void increment_game_stats(const uint64_t bet_amt, const uint64_t won_amt) {
		auto globalvars_itr = globalvars.find(2); // 2 - Total amount
		globalvars.modify(globalvars_itr, _self, [&](auto &g) { g.val += bet_amt; });

		if (won_amt > 0) {
			globalvars_itr = globalvars.find(3); // Total won ammount
			globalvars.modify(globalvars_itr, _self, [&](auto &g) { g.val += won_amt; });
		}
	}

	// Airdrops tokens.
	void airdrop(const uint64_t bet_id, const uint64_t bet_amt, const account_name bettor) {
		uint64_t drop_amt = (1 * bet_amt) / 30;
		const uint64_t bet_token_balance = get_balance(N(betdividends), symbol_type(S(4, BET)));

		if (bet_token_balance == 0)
			return;
		else if (bet_token_balance < drop_amt)
			drop_amt = bet_token_balance;

		action(
			permission_level{_self, N(active)},
			N(betdividends),
			N(bet),
			make_tuple(
					_self,
					bettor,
					asset(drop_amt, symbol_type(S(4, BET))),
					string("Bet id: ") + to_string(bet_id) + string(" - airdrop -")))
			.send();
	}

	// Returns token balance.
	uint64_t get_balance(const account_name token_contract, const symbol_type &token_type) const {
		accounts from_accounts(token_contract, _self);

		const auto token_name = token_type.name();
		auto my_account_itr = from_accounts.find(token_name);

		if (my_account_itr == from_accounts.end())
			return 0;

		const asset my_balance = my_account_itr->balance;
		return (uint64_t)my_balance.amount;
	}

	uint64_t get_payout(const uint64_t roll_under, const uint64_t commission) const {
		return ((10000 - commission) * 100) / (roll_under - 1);
	}

	// User can't win more than (eos_balance - liabilities) / 25.
	uint64_t get_max_win() const {
		const uint64_t eos_balance = get_balance(N(eosio.token), symbol_type(S(4, EOS)));

		auto liabilities_struct = globalvars.get(4); // Liabilities amount
		const uint64_t liabilities = liabilities_struct.val;

		return (eos_balance - liabilities) / 25;
	}
};

EOSIO_ABI_EX(WinCasinoEOS,
						 (initcontract)(updaterandkey)(bet)(refundbet)(checkbet)(removebet)(betsender))
