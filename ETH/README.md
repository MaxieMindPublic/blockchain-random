## Idea

This smart contract contains the function of finding a random gaming combination using the hash of the next block of the 
ETH network. This smart contract shows the possibility of correct and balanced generation of a gaming combination.