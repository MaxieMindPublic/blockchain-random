pragma solidity ^0.4.24;

contract Random {
    uint public blockNumber;
    uint public oldBlockNumber;
    bytes32 public blockHash;
 
    bytes public win;
    
    modifier isNewHash() {
        require(blockNumber - 1 > oldBlockNumber);
        _;
    }
    
    function getRandom(uint count, uint maxCount) public {
        blockNumber = block.number;
        blockHash = blockhash(blockNumber - 1);
        getWinNumbers(count, maxCount);
    }
    
    function getCurrentBlock() public view returns(uint) {
        return block.number;
    }
    
    function getWinNumbers(uint count, uint maxCount) isNewHash private {
        bytes32 hashRandom = keccak256(abi.encodePacked(blockHash));
        bytes memory numbers = new bytes(maxCount);
        bytes memory winNumbers = new bytes(count);
        
        for (uint i = 0; i < maxCount; i++) {
            numbers[i] = byte(i + 1);
        }
        
        for (i = 0; i < count; i++) {
            uint n = maxCount - i;
            uint r = (uint (hashRandom[i * 4]) + (uint (hashRandom[i * 4 + 1]) << 8) + 
            (uint (hashRandom[i * 4 + 2]) << 16) + (uint (hashRandom[i * 4 + 3]) << 24)) % n;
            
            winNumbers[i] = numbers[r];
            numbers[r] = numbers[n - 1];
        }
        
        win = winNumbers;
        oldBlockNumber = blockNumber;
    }
}
